Replace(
    Replace(
        STUFF(
            (SELECT CAST(
                ', ' + tblArtWork.edit_URL + ', ' + tblArtWork.artLogNum_dashes + '-' + tblJob.jobArtworkVersion + ' (' + tblJob.imprintLocation + ') (' + Ltrim(Str(tblJob.quantity)) + ')'  AS VARCHAR(MAX)
                ) 
            FROM tblJob INNER JOIN tblArtWork ON tblJob.jobArtLogNum_noLetter = tblArtWork.artLogNum
            WHERE workOrderID='[@field:tblProof_workOrder_id]' AND tblJob.status = 1 AND tblJob.artLogNum IS NOT NULL
            FOR XML PATH ('')
            ), 1, 2, ''
        ),', ,',''
    ),' ,',''
)
