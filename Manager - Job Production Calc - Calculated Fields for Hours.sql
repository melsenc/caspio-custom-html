--Calculated Field 6 (Stitch Count)
    
    /*Set a minimum stitch count of 4000 stitches for all EMB jobs*/
    CASE

    WHEN [@field:tblJob_jobDept] = 'EMB' AND IsNull([@field:tblJob_stitchCount], 0) < 4000
    THEN 4000

    WHEN [@field:tblJob_jobDept] = 'EMB' AND IsNull([@field:tblJob_stitchCount], 0) >= 4000
    THEN [@field:tblJob_stitchCount]

    ELSE Null

    END

--Calculated Field 7 (EMB Heads)

    CASE

    WHEN [@field:tblJob_jobDept] = 'EMB' AND Len('[@field:tblJob_machine_group]') = 0
    THEN 1

    WHEN [@field:tblJob_jobDept] = 'EMB' AND Len('[@field:tblJob_machine_group]') > 0
    THEN Cast((Left('[@field:tblJob_machine_group]',1)) AS int) 
    /*Because all EMB machine names start with an integer representing the number of embroidery heads on the machine,
    this formula converts the first character to an integer, then uses the integer to return the number of heads*/

    ELSE Null

    END

--Calculated Field 8 (Quantity)

    IsNull([@field:tblJob_quantity], 0)

--Calculated Field 10 (SP Setup Minutes)

    CASE

    WHEN [@field:tblJob_jobType] = 'SP'

    THEN 7+IsNull([@field:tblJob_numScreens#],0)*5

    ELSE 0

    END

--Calculated Field 11 (Hours)

    CASE

    WHEN [@field:tblJob_jobType] = 'ENAM' OR [@field:tblJob_jobType] = 'ENMB'
    THEN [@calcfield:8]/10

    WHEN [@field:tblJob_jobType] = 'EMB' OR [@field:tblJob_jobType] = 'ETWL'
    THEN [@calcfield:8]*[@calcfield:6]/([@calcfield:7]*25000)

    WHEN [@field:tblJob_jobType] = 'EMP'
    THEN [@calcfield:8]*[@calcfield:6]/([@calcfield:7]*25000) + [@calcfield:8]/10

    WHEN [@field:tblJob_jobType] = 'SP'
    THEN [@calcfield:8]*[@calcfield:9]/390 + [@calcfield:10]/60

    WHEN [@field:tblJob_jobDept] = 'TRF'
    THEN [@calcfield:8]/40

    ELSE 0

    END

--Formula explanations:

/* 

ENAM/ENMB:
10 personalizations per hour

EMB/ETWL:
( (Quantity * (Number of stitches/1000) * 2.4) / number of heads ) / 60
But I simplified it down to just one number besides the three variables of quantity, stitches, and heads

EMP:
The EMB/ETWL component added to the personalization component

SP:
Production Time is based on the following:
Set up time = 12 minutes for the first screen and 5 minutes for every other screen.
Run Time is based on 390 prints per hour/# of Cycles

One thing to keep in mind about the SP formula is that it is design to model AUTO jobs. 
At this point, we do not have a specific formula for MANUAL jobs.
We also do not account for the extra time involved with fleece items, 
though we have started to keep track of fleece quantity.

TRF:
40 applications per hour

*/