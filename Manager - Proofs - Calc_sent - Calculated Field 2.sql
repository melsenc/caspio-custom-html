CASE

WHEN IsNull('[@field:tblProof_message_last_sender]', '') = '' THEN ''

WHEN '[@field:tblProof_message_last_sender]' = '[@authfield:tblUser_userID]' THEN 'sent'

ELSE 'incoming'

END