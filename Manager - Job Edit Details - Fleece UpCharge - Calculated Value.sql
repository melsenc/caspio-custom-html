CASE

WHEN [@field:jobType] = 'SP' THEN [@field:fleeceQuantity]*
(
    SELECT lookup_level_cost.fleece_upcharge

    FROM ((lookup_level_cost INNER JOIN tblCustomer ON lookup_level_cost.level_id = tblCustomer.level_id) INNER JOIN tblWorkOrder ON tblCustomer.custCode = tblWorkOrder.custCode) INNER JOIN tblJob ON tblWorkOrder.workOrder_id = tblJob.workOrderID

    WHERE tblJob.jobID = [@field:jobID]
)

ELSE 0

END