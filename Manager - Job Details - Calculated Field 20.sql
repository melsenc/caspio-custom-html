Replace(
    Replace(
        Replace(
            Replace(
                STUFF(
                    (SELECT DISTINCT CAST(
                        ', ' + ISNULL(tblInkThread.colorRGB, '#E4F0F8') + ', ' + tblInkThread.colorName + ' - ' + tblProductionData_SP.colorNotes AS VARCHAR(MAX)
                        ) 
                    FROM tblInkThread INNER JOIN tblProductionData_SP ON tblInkThread.colorID = tblProductionData_SP.color_id
                    WHERE tblProductionData_SP.job_id = [@field:jobID] 
                    AND tblInkThread.colorID <> 5706
                    AND tblInkThread.colorID <> 5395
                    AND tblInkThread.colorID <> 5331
                    AND tblInkThread.colorID <> 5329
                    AND tblInkThread.colorID <> 5454
                    AND tblInkThread.colorID <> 5697
                    AND tblInkThread.colorID <> 5698
                    AND tblInkThread.colorID <> 5699
                    AND tblInkThread.colorID <> 4834
                    FOR XML PATH ('')
                    ), 1, 2, ''
                ),' - ,',','
            ),', ,',''
        ),' ,',''
    ),' - ',' '
)