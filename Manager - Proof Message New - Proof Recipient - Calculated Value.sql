SELECT tblUser.userFullName

FROM tblUser 
INNER JOIN tblProof
ON tblUser.userName = tblProof.custContact_userName

WHERE tblProof.proof_key = [@field:messages_proof_key]