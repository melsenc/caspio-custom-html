Replace(
    Replace(
        Replace(
            Replace(
                STUFF(
                    (SELECT DISTINCT CAST(
                        ', ' + ISNULL(tblInkThread.colorRGB, '#E4F0F8') + ', ' + tblInkThread.colorName AS VARCHAR(MAX)
                        ) 
                    FROM tblInkThread WHERE tblInkThread.colorName = [@field:tblJob_twill_01] 
                    FOR XML PATH ('')
                    ), 1, 2, ''
                ),', ,',''
            ),' ,',''
        ) + ', ' +
        Replace(
            Replace(
                STUFF(
                    (SELECT DISTINCT CAST(
                        ', ' + ISNULL(tblInkThread.colorRGB, '#E4F0F8') + ', ' + tblInkThread.colorName AS VARCHAR(MAX)
                        ) 
                    FROM tblInkThread WHERE tblInkThread.colorName = [@field:tblJob_twill_02] 
                    FOR XML PATH ('')
                    ), 1, 2, ''
                ),', ,',''
            ),' ,',''
        ) + ', ' +
        Replace(
            Replace(
                STUFF(
                    (SELECT DISTINCT CAST(
                        ', ' + ISNULL(tblInkThread.colorRGB, '#E4F0F8') + ', ' + tblInkThread.colorName AS VARCHAR(MAX)
                        ) 
                    FROM tblInkThread WHERE tblInkThread.colorName = [@field:tblJob_twill_03]
                    FOR XML PATH ('')
                    ), 1, 2, ''
                ),', ,',''
            ),' ,',''
        ),', ,',''
    ),' ,',''
)