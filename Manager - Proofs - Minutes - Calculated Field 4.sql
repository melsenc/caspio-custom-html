CASE

WHEN [@field:tblProof_proofStatus] = 0
THEN IsNull([@field:tblProof_ArtMinutes], 20)

WHEN [@field:tblProof_proofStatus] = 3
THEN 15 + IsNull([@field:tblProof_FilmMinutes], 5) * IsNull([@calcfield:3], 0)

WHEN [@field:tblProof_proofStatus] = 7
THEN 5 + IsNull([@field:tblProof_FilmMinutes], 5) * IsNull([@calcfield:3], 0)

WHEN [@field:tblProof_proofStatus] = 4
THEN IsNull([@field:tblProof_ArtMinutes], 20) + 15 + IsNull([@field:tblProof_FilmMinutes], 5) * IsNull([@calcfield:3], 0)

ELSE 0

END
