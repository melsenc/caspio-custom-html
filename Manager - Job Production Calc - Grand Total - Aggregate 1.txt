CASE

WHEN

Len(cast((SUM(Convert(int,[@calcfield:1]))+cast(SUM(Convert(int,[@calcfield:3])+[@calcfield:10])/60 as int)) as varchar)) = 1 
AND
Len(cast((SUM(Convert(int,[@calcfield:3])+[@calcfield:10])-cast(SUM(Convert(int,[@calcfield:3])+[@calcfield:10])/60 as int)*60) as varchar)) = 1

THEN
'0' +
cast(
    (SUM(
        Convert(int,[@calcfield:1])
    ) + 
    cast(
        SUM(
            Convert(int,[@calcfield:3]) + [@calcfield:10]
        )/60
    as int)
    ) 
as varchar) + 
':0' + 
cast(
    (SUM(
        Convert(int,[@calcfield:3]) + [@calcfield:10]
    ) - 
    cast(
        SUM(
            Convert(int,[@calcfield:3]) + [@calcfield:10]
        )/60 
    as int)*60) 
as varchar)

WHEN

Len(cast((SUM(Convert(int,[@calcfield:1]))+cast(SUM(Convert(int,[@calcfield:3])+[@calcfield:10])/60 as int)) as varchar)) = 1 
AND
Len(cast((SUM(Convert(int,[@calcfield:3])+[@calcfield:10])-cast(SUM(Convert(int,[@calcfield:3])+[@calcfield:10])/60 as int)*60) as varchar)) = 2

THEN
'0' +
cast(
    (SUM(
        Convert(int,[@calcfield:1])
    ) + 
    cast(
        SUM(
            Convert(int,[@calcfield:3]) + [@calcfield:10]
        )/60
    as int)
    ) 
as varchar) + 
':' + 
cast(
    (SUM(
        Convert(int,[@calcfield:3]) + [@calcfield:10]
    ) - 
    cast(
        SUM(
            Convert(int,[@calcfield:3]) + [@calcfield:10]
        )/60 
    as int)*60) 
as varchar)

WHEN

Len(cast((SUM(Convert(int,[@calcfield:1]))+cast(SUM(Convert(int,[@calcfield:3])+[@calcfield:10])/60 as int)) as varchar)) = 2 
AND
Len(cast((SUM(Convert(int,[@calcfield:3])+[@calcfield:10])-cast(SUM(Convert(int,[@calcfield:3])+[@calcfield:10])/60 as int)*60) as varchar)) = 1

THEN
cast(
    (SUM(
        Convert(int,[@calcfield:1])
    ) + 
    cast(
        SUM(
            Convert(int,[@calcfield:3]) + [@calcfield:10]
        )/60
    as int)
    ) 
as varchar) + 
':0' + 
cast(
    (SUM(
        Convert(int,[@calcfield:3]) + [@calcfield:10]
    ) - 
    cast(
        SUM(
            Convert(int,[@calcfield:3]) + [@calcfield:10]
        )/60 
    as int)*60) 
as varchar)

WHEN

Len(cast((SUM(Convert(int,[@calcfield:1]))+cast(SUM(Convert(int,[@calcfield:3])+[@calcfield:10])/60 as int)) as varchar)) = 2 
AND
Len(cast((SUM(Convert(int,[@calcfield:3])+[@calcfield:10])-cast(SUM(Convert(int,[@calcfield:3])+[@calcfield:10])/60 as int)*60) as varchar)) = 2

THEN
cast(
    (SUM(
        Convert(int,[@calcfield:1])
    ) + 
    cast(
        SUM(
            Convert(int,[@calcfield:3]) + [@calcfield:10]
        )/60
    as int)
    ) 
as varchar) + 
':' + 
cast(
    (SUM(
        Convert(int,[@calcfield:3]) + [@calcfield:10]
    ) - 
    cast(
        SUM(
            Convert(int,[@calcfield:3]) + [@calcfield:10]
        )/60 
    as int)*60) 
as varchar)

END

-------------------------

[runHours] = [@calcfield:1]
[setupTime] = [@calcfield:2]
[runMinutes] = [@calcfield:3]
[setupMinutes-int] = [@calcfield:10]
