Replace(
    STUFF(
            (SELECT DISTINCT CAST(
                ', ' + Ltrim(Str(artFile_id)) + ', ' + artFile_filename + ', ' + artFile_URL + ', cdn.caspio.com/1DAE6000' + Replace(Replace(Replace(Replace(Replace(artFile_filename, char(32), '%20'),char(39),'%27'),char(38),'%26'),char(35),'%23'),char(43),'%2B')  AS VARCHAR(MAX)
                ) 
            FROM tblArtFile
            WHERE message_id='[@field:Messages_messages_id]'
            FOR XML PATH ('')
            ), 1, 2, ''
    ), 'cdn.caspio.com/1DAE6000,',','
)