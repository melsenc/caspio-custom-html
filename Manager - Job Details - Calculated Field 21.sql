Replace(
    Replace(
        STUFF(
            (SELECT DISTINCT CAST(
                ', ' + ISNULL(tblInkThread.colorRGB, '#E4F0F8') + ', ' + tblInkThread.colorName AS VARCHAR(MAX)
                ) 
            FROM tblInkThread INNER JOIN tblProductionData_EMB ON tblInkThread.colorID = tblProductionData_EMB.color_id
            WHERE tblProductionData_EMB.job_id = [@field:jobID] 
            AND tblInkThread.colorID <> 5706
            AND tblInkThread.colorID <> 5395
            AND tblInkThread.colorID <> 5331
            AND tblInkThread.colorID <> 5329
            AND tblInkThread.colorID <> 5454
            AND tblInkThread.colorID <> 5697
            AND tblInkThread.colorID <> 5698
            AND tblInkThread.colorID <> 5699
            AND tblInkThread.colorID <> 4834
            FOR XML PATH ('')
            ), 1, 2, ''
        ),', ,',''
    ),' ,',''
)