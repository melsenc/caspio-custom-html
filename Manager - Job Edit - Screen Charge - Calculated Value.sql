CASE

WHEN (
    SELECT custCode FROM tblWorkOrder WHERE workOrder_id = [@field:workOrderID]
) = 'BX'
THEN 0

WHEN (Try_Convert(integer, [@field:oversizeScreen]) = 1) AND (Try_Convert(integer, [@field:setupReq]) = 0) AND ([@field:jobType] = 'SP') AND (Try_Convert(integer, [@field:jobReorder]) = 0) THEN [@field:numColors]*
(
    SELECT lookup_level_cost.oversize_screen_cost

    FROM (lookup_level_cost INNER JOIN tblCustomer ON lookup_level_cost.level_id = tblCustomer.level_id) INNER JOIN tblWorkOrder ON tblCustomer.custCode = tblWorkOrder.custCode

    WHERE workOrder_id = [@field:workOrderID]
)

WHEN (Try_Convert(integer, [@field:oversizeScreen]) = 0) AND (Try_Convert(integer, [@field:setupReq]) = 1) AND ([@field:jobType] = 'SP') AND (Try_Convert(integer, [@field:jobReorder]) = 0) THEN [@field:numColors]*
(
    SELECT lookup_level_cost.setup_cost

    FROM (lookup_level_cost INNER JOIN tblCustomer ON lookup_level_cost.level_id = tblCustomer.level_id) INNER JOIN tblWorkOrder ON tblCustomer.custCode = tblWorkOrder.custCode

    WHERE workOrder_id = [@field:workOrderID]
)

WHEN (Try_Convert(integer, [@field:oversizeScreen]) = 0) AND (Try_Convert(integer, [@field:setupReq]) = 0) AND ([@field:jobType] = 'SP') AND (Try_Convert(integer, [@field:jobReorder]) = 0) THEN [@field:numColors]*
(
    SELECT lookup_level_cost.screen_cost

    FROM (lookup_level_cost INNER JOIN tblCustomer ON lookup_level_cost.level_id = tblCustomer.level_id) INNER JOIN tblWorkOrder ON tblCustomer.custCode = tblWorkOrder.custCode

    WHERE workOrder_id = [@field:workOrderID]
)

ELSE 0

END