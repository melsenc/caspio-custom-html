CASE

WHEN IsNull([@field:tblProof_ScreensOutput], 0) = 0 AND [@field:tblProof_proofStatus] = 7

THEN
Cast((SELECT SUM(tblJob.numScreens) 
FROM tblJob
WHERE workOrderID='[@field:tblProof_workOrder_id]' AND tblJob.jobType = 'SP'
FOR XML PATH ('')) AS int)

WHEN IsNull([@field:tblProof_ScreensOutput], 0) = 0

THEN
Cast((SELECT SUM(tblJob.numScreens) 
FROM tblJob
WHERE workOrderID='[@field:tblProof_workOrder_id]' AND tblJob.jobType = 'SP' AND tblJob.jobReorder = 0
FOR XML PATH ('')) AS int)

ELSE [@field:tblProof_ScreensOutput]

END
