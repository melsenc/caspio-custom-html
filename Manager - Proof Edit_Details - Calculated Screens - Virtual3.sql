CASE

WHEN [@field:tblProof_proofStatus] = 7

THEN
Cast((SELECT SUM(tblJob.numScreens) 
FROM tblJob
WHERE workOrderID='[@field:tblProof_workOrder_id]' AND tblJob.jobType = 'SP'
FOR XML PATH ('')) AS int)

ELSE
Cast((SELECT SUM(tblJob.numScreens) 
FROM tblJob
WHERE workOrderID='[@field:tblProof_workOrder_id]' AND tblJob.jobType = 'SP' AND tblJob.jobReorder = 0
FOR XML PATH ('')) AS int)

END
