CASE

WHEN [@field:tblProof_proofStatus] = 0
THEN IsNull([@field:tblProof_ArtMinutes], 20)

WHEN [@field:tblProof_proofStatus] = 3
THEN 15 + IsNull([@field:tblProof_FilmMinutes], 5) * IsNull((
  CASE

  WHEN IsNull([@field:tblProof_ScreensOutput], 0) = 0

  THEN
  Cast((SELECT SUM(tblJob.numScreens) 
  FROM tblJob
  WHERE workOrderID='[@field:tblProof_workOrder_id]' AND tblJob.jobType = 'SP' AND tblJob.jobReorder = 0
  FOR XML PATH ('')) AS int)

  ELSE [@field:tblProof_ScreensOutput]

  END
),0)

WHEN [@field:tblProof_proofStatus] = 7
THEN 5 + IsNull([@field:tblProof_FilmMinutes], 5) * IsNull((
  CASE

  WHEN IsNull([@field:tblProof_ScreensOutput], 0) = 0

  THEN
  Cast((SELECT SUM(tblJob.numScreens) 
  FROM tblJob
  WHERE workOrderID='[@field:tblProof_workOrder_id]' AND tblJob.jobType = 'SP' 
  FOR XML PATH ('')) AS int)

  ELSE [@field:tblProof_ScreensOutput]

  END
),0)

WHEN [@field:tblProof_proofStatus] = 4
THEN IsNull([@field:tblProof_ArtMinutes], 20) + 15 + IsNull([@field:tblProof_FilmMinutes], 5) * IsNull((
  CASE

  WHEN IsNull([@field:tblProof_ScreensOutput], 0) = 0

  THEN
  Cast((SELECT SUM(tblJob.numScreens) 
  FROM tblJob
  WHERE workOrderID='[@field:tblProof_workOrder_id]' AND tblJob.jobType = 'SP' AND tblJob.jobReorder = 0
  FOR XML PATH ('')) AS int)

  ELSE [@field:tblProof_ScreensOutput]

  END
), 0)

ELSE 0

END
