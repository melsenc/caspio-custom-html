SELECT tblCustomer.custName 
FROM tblCustomer 
INNER JOIN tblWorkOrder 
ON tblCustomer.custCode = tblWorkOrder.custCode
WHERE tblWorkOrder.workOrder_id = [@field:workOrderID]