CASE

WHEN
IsNull((SELECT head FROM tblProductionData_SP WHERE job_id = [@field:tblJob_jobID] AND screen_number = [@field:tblSeparation_screen_number]), 0) > 0

THEN

Replace(
(SELECT cycle1_order FROM tblProductionData_SP WHERE job_id = [@field:tblJob_jobID] AND screen_number = [@field:tblSeparation_screen_number])+','+(SELECT cycle2_order FROM tblProductionData_SP WHERE job_id = [@field:tblJob_jobID] AND screen_number = [@field:tblSeparation_screen_number])+','+(SELECT cycle3_order FROM tblProductionData_SP WHERE job_id = [@field:tblJob_jobID] AND screen_number = [@field:tblSeparation_screen_number])+','+(SELECT cycle4_order FROM tblProductionData_SP WHERE job_id = [@field:tblJob_jobID] AND screen_number = [@field:tblSeparation_screen_number]),
'0', '')

ELSE
Str((SELECT screen_number FROM tblProductionData_SP WHERE job_id = [@field:tblJob_jobID] AND screen_number = [@field:tblSeparation_screen_number]))

END
