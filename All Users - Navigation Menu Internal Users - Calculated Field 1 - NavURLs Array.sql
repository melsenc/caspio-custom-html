Replace(
    Replace(
        STUFF(
            (SELECT DISTINCT CAST(
                ', ' + tblURL.element_ID + ', ' + tblURL.URL_address + ', ' + tblURL.target + ', ' + tblURL.element_value AS VARCHAR(MAX)
                ), tblURL.nav_sort
            FROM tblURL INNER JOIN tblUserNavURL ON tblURL.URL_id = tblUserNavURL.url_ID
            WHERE tblUserNavURL.authentication_class='[@field:tblUser_userAccess]'
            ORDER BY tblURL.nav_sort
            FOR XML PATH ('')
            ), 1, 2, ''
        ),', ,',''
    ),' ,',''
)